angular.module('starter.services', [])

.factory('locationProvider', ['$q','$ionicPopup',function($q,$ionicPopup) {
var posOptions = {timeout: 10000, enableHighAccuracy: false};
var locationProvider=this;
this.longitude='';
this.latitude='';
this.resolve='',
this.reject = '',
this.onPositionLoad= function (position) {

     
   console.log('Latitude: '          + position.coords.latitude          + '\n' +
              'Longitude: '         + position.coords.longitude         + '\n' +
              'Altitude: '          + position.coords.altitude          + '\n' +
              'Accuracy: '          + position.coords.accuracy          + '\n' +
              'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
              'Heading: '           + position.coords.heading           + '\n' +
              'Speed: '             + position.coords.speed             + '\n' +
              'Timestamp: '         + position.timestamp                + '\n');

$ionicPopup.alert({
     title: 'loaded',
     template: 'No Connection Found.'
   });
    locationProvider.resolve({lng:position.coords.longitude,lat:position.coords.latitude});

  };

this.onPositionLoadError =function (error) {
    $ionicPopup.alert({
    title: error.toString()+'error',
     template: 'No Connection Found.'
   });
        locationProvider.reject({longitude:'0',latitude:'0'});
};


this.getCurrentPosition = function () {
    
    return $q(function(resolve, reject) {
    locationProvider.resolve = resolve;
    locationProvider.reject=reject;
   console.log('getting current position');

   try{
    navigator.geolocation.getCurrentPosition(locationProvider.onPositionLoad,locationProvider.onPositionLoadError,{maximumAge:60000, timeout:20000, enableHighAccuracy:true});
   }catch(e){
$ionicPopup.alert({
     title: 'error'+e,
     template: 'No Connection Found.'
   });
  }
  });

}


return {location:this.getCurrentPosition()};

}]);