


angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
})

.controller('MapCtrl', function($scope,locationProvider,$ionicPopup) {
//var mymap = L.map('mapid').setView([51.505, -0.09], 13);
//console.log(mymap);
//console.log(locationProvider.longitude);
//var map = L.map('mapid').setView([51.505, -0.09], 13);
//{lng:position.coords.longitude,lat:position.coords.latitude}
locationProvider.location.then(function(location) {


 $ionicPopup.alert({
     title: location.lat + location.lng,
     template: 'No Connection Found.'
   });
 
 

  console.log('Success: ' + location.longitude);
  var map = L.map('mapid').setView([location.lat,location.lng], 13);;
L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);
    L.marker(location).addTo(map);


}, function(reason) {
  console.log('Failed: ' + reason);
});





});



